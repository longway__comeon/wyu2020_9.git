// miniprogram/pages/index/index.js
import Dialog from '@vant/weapp/dialog/dialog';
var appInstance = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 1,
    hei: null,
    maxTop: null,
    tdHeight: null,
    tdWidth: null,
    tableX: ['', '日', '一', '二', '三', '四', '五', '六'],
    classData: [],
    indexWeek: null, //第几周
    dialogShow: false,
    yzmValue: "",
    yzmSrc: "",
    upmeunShow: false,
    columns1: ['第一周', '第二周', '第三周', '第四周', '第五周', '第六周', '第七周', '第八周', '第九周', '第十周', '第十一周', '第十二周', '第十三周', '第十四周', '第十五周', '第十六周', '第十七周', '第十八周', '第十九周', '第二十周'],
    //     columns1: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],

    week: "", //显示周次
    columns2: [], //学期
    radio: "2", //判断重新导入
    selectTerm: "",
    showSelect: false,
    showPicker: false,
    showYzm: false
  },

  // 
  tdDialog(e) { //课表dialog
    console.log(e.currentTarget.dataset.data);
    let data = e.currentTarget.dataset.data
    Dialog.alert({
      title: data.name,
      message: `上课地点 => ${data.destination?data.destination:'(空)'}
        授课教师 => ${data.teacher}
        教学班名称 => ${data.classmate}
        人数 => ${data.mateNum}
        上课类型 => ${data.type}
        授课内容 => ${data.context?data.context:'(空)'}`,
    }).then();
  },
  cxdr() {
    this.setData({
      showSelect: true
    })
  },
  getNew() {
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    if (appInstance.login) {
      wx.hideLoading()
      this.setData({
        showPicker: true
      })
    } else {
      wx.cloud.callFunction({ //获取验证码
        name: 'login',
        data: {
          skyw: true
        }
      }).then(res => {
        wx.hideLoading()
        this.setData({
          yzmSrc: res.result.data,
          showYzm: true
        })
      }).catch(err => {
        wx.hideLoading()
        Dialog.alert({
          message: "网络错误，请重试",
        })
        appInstance.login = false
      })
    }
  },
  selectWeek() { //上拉列表显示
    this.setData({
      upmeunShow: true
    });
  },
  select(event) { //上拉列表选择后
    const {
      value,
      index
    } = event.detail;
    // console.log(value, index);
    this.setData({
      upmeunShow: false,
      week: value,
      indexWeek: index
    });
  },
  closeMeun() { //上拉列表隐藏
    this.setData({
      upmeunShow: false
    });
  },
  pickerChange(event) { //选择学期变化
    this.setData({
      selectTerm: event.detail.value
    })
  },
  radioChange(event) {
    this.setData({
      radio: event.detail,
    });
  },
  tabbarChange(event) { //标签栏变化
    // event.detail 的值为当前选中项的索引
    this.setData({
      active: event.detail
    });
    if (event.detail == 0) {
      wx.redirectTo({
        url: '../home/home'
      })
    } else if (event.detail == 1) {
      wx.redirectTo({
        url: '../classTable/classTable'
      })
    } else if (event.detail == 2) {
      wx.redirectTo({
        url: '../person/person'
      })
    }

  },
  yzmChange(event) {
    this.setData({
      yzmValue: event.detail
    })
  },
  submit0() {
    if (this.data.radio == "1") { //新数据
      this.getNew()
    } else {
      this.setData({
        showPicker: true
      })
    }
  },
  submit1() {
    wx.showLoading({
      title: '登陆中......',
      mask: true
    })

    wx.cloud.callFunction({ //登陆
      name: 'login',
      data: {
        yzmValue: this.data.yzmValue
      }
    }).then(res => {
      wx.hideLoading()
      if (typeof res.result == "string") { //登陆失败
        if (res.result == "验证码不正确") {
          Dialog.alert({
            message: res.result,
          }).then(() => {

            wx.showLoading({
              title: '更新验证码中......',
              mask: true
            })
            wx.cloud.callFunction({ //获取验证码
              name: 'login',
            }).then(res => {
              wx.hideLoading()
              this.setData({
                yzmSrc: res.result.data,
                showYzm: true
              })
            }).catch(err => Dialog.alert({
              message: "网络错误，请重试",
            }))

          });
        } else { //其它错误
          Dialog.alert({
            message: res.result,
          })
        }

      } else { //登陆成功
        appInstance.login = true;
        this.setData({
          showPicker: true
        })
      }
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: "网络错误，请重试",
      })
      appInstance.login = false
    })
  },
  submit2() { //发送重新导入
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    if (this.data.radio == "1") { //新数据
      wx.cloud.callFunction({
        name: 'skrw',
        data: {
          getNew: true
        }
      }).then(res => {
        // console.log(res);
        this.getkb(); //获取课表
        wx.hideLoading()
      }).catch(err => {
        wx.hideLoading()
        Dialog.alert({
          message: '网络错误，请重试',
        })
        appInstance.login = false
      })
    } else {
      this.getkb()
    }

  },
  getkb() {
    wx.showLoading({
      title: '生成课表中......',
      mask: true
    })
    wx.cloud.callFunction({ //生成课表
      name: 'kb',
      data: {
        selectTerm: this.data.selectTerm
      }
    }).then(res => {
      wx.hideLoading()
      if (res.result != 404) {
        this.setData({
          classData: res.result
        })
      }
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: "网络错误，请重试",
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    const that = this;
    // 获取学期
    await wx.cloud.callFunction({ //获取验证码
      name: 'user',
      data: {
        type: "getselectTerm"
      }
    }).then(res => {
      this.setData({
        selectTerm: res.result, //要改成0
      })
    })

    // 判断时间-->第几周
    // let date1 = new Date(2020, 9, 14);
    // let date2 = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate());
    // let dayNum = (date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24); /*不用考虑闰年否*/
    // let weekNum = (dayNum < 6 ? 1 : Math.ceil(dayNum / 6)) - 1


    let colData = []
    for (let index = 0; index < 4; index++) {
      let date = new Date().getFullYear() - index;
      colData.push(date + "02");
      colData.push(date + "01");
    }

    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
      indexWeek: 0, //weekNum
      week: this.data.columns1[0],
      columns2: colData,
    })
    this.getkb()

    wx.getSystemInfo({
      success(res) {
        that.setData({
          tdHeight: (res.windowHeight - 50 - appInstance.hei) / 13,
          tdWidth: res.windowWidth / 8
        })
      }
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.cloud.callFunction({ //获取验证码
      name: 'user',
      data: {
        type: "changeselectTerm",
        selectTerm: this.data.selectTerm
      }
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})