// miniprogram/pages/index/index.js
import Dialog from '@vant/weapp/dialog/dialog';
var appInstance = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hei: null,
    maxTop: null,
    xh: "",
    mm: "",
    yzm: "",
    schoolList: [{
        text: '五邑大学',
        value: 0
      },
      {
        text: '佛山科技学院',
        value: 1
      },
      {
        text: '仲恺农业工程学院',
        value: 2
      },
    ],
    selectSchool: 0,
    checked: false,
    yzmSrc: "",
    disabled: true
  },
  select(event) { //选择学校,改变selectSchool值
    this.setData({
      selectSchool: event.detail
    })
  },
  check(event) { //选择是否授权
    this.setData({
      checked: event.detail,
    });
    this.onWatch()

  },
  input(event) { //input输入，改变xh、mm、yzm的值
    let type = event.target.dataset.type
    if (type == "xh") {
      this.setData({
        xh: event.detail
      })
    } else if (type == "mm") {
      this.setData({
        mm: event.detail
      })
    } else {
      this.setData({
        yzm: event.detail
      })
    }
    this.onWatch()

  },
  login(event) { //登陆
    wx.showLoading({
      title: '登陆中......',
      mask: true
    })
    wx.cloud.callFunction({//上传学号、密码、验证码
      name: 'login',
      data: {
        yzmValue: this.data.yzm,
        xhValue: this.data.xh,
        mmValue: this.data.mm
      }
    }).then(res => {
      wx.hideLoading()
      if (typeof res.result == "string") { //登陆失败
        Dialog.alert({
          message: res.result,
        }).then(() => {
          wx.showLoading({
            title: '更新验证码中......',
            mask: true
          })
          this.getYzm();
        });
      } else { //登陆成功
        appInstance.login = true;//改变登陆状态
        wx.redirectTo({
          url: '../home/home'
        })
      }
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: "网络错误，请重试",
      })
    })
  },
  onWatch(event) { //监听数据是否完整
    if (this.data.xh != "" && this.data.mm != "" && this.data.yzm != "" && this.data.checked) {//完整则允许按钮可以按
      this.setData({
        disabled: false
      })
    }
  },
  getYzm() {
    wx.cloud.callFunction({ //获取验证码
      name: 'login',
      data: {
        getyzm: true  //
      }
    }).then(res => {
      if (res.result == "ok") {
        wx.redirectTo({
          url: '../home/home'
        })
      } else {
        wx.hideLoading()
        this.setData({
          yzmSrc: res.result.data,
        })
      }
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: "网络错误，请重试",
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中......',
      mask: true
    })
    this.getYzm();//判断是否有学号信息

    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})