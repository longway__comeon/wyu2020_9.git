var appInstance = getApp()
import Toast from '@vant/weapp/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    hei: null,
    maxTop: null,
  },
  tabbarChange(event) { //标签栏变化
    // event.detail 的值为当前选中项的索引
    this.setData({
      active: event.detail
    });
    if (event.detail == 0) {
      wx.redirectTo({
        url: '../home/home'
      })
    } else if (event.detail == 1) {
      wx.redirectTo({
        url: '../classTable/classTable'
      })
    } else if (event.detail == 2) {
      wx.redirectTo({
        url: '../person/person'
      })
    }

  },
  nodo() {
    Toast('模块正在建设中~~');
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})