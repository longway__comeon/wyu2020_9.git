var appInstance = getApp()
import Toast from '@vant/weapp/toast/toast';
import Dialog from '@vant/weapp/dialog/dialog';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hei: null,
    maxTop: null,
    dataList: [],
    headerShow: false,
    dialogShow: false,
    option1: null, //学期数组
    option2: [], //周次数组
    option3: [], //周几数组
    value1: 0, //学期
    value2: 0, //周次
    value3: 0, //周几
    name: "", //课程名称
    teacher: "" //教师
  },


  change1(e) { //学期
    this.data.value1 = e.detail
  },
  change2(e) { //周次
    this.data.value2 = e.detail
  },
  change3(e) { //周几
    this.data.value3 = e.detail
  },
  nameChange(e) { //课程名称
    this.data.name = e.detail
  },
  teacherChange(e) { //教师
    this.data.teacher = e.detail
  },
  submit() {
    wx.showLoading({
      title: '查询中......',
      mask: true
    })

    wx.cloud.callFunction({
      name: 'yqck',
      data: {
        term: this.data.option1[this.data.value1],
        date: this.data.option2[this.data.value2],
        day: this.data.option3[this.data.value3],
        name: this.data.name,
        teacher: this.data.teacher
      }
    }).then(res => {
      wx.hideLoading()
      if (res.result == 404 || res.result.data.length == 0) {
        Toast.fail("没有找到数据")
      } else {
        this.setData({
          dataList: res.result.data
        })
      }
    })

    this.setData({ //隐藏
      headerShow: true
    })
  },
  btnTap() {
    this.setData({ //显示
      headerShow: !this.data.headerShow
    })
  },
  itemTap(e) { //弹窗显示数据
    let data = e.currentTarget.dataset.data
    Dialog.alert({
      title: data.name,
      message: `上课地点 => ${data.destination?data.destination:'(空)'}
        授课教师 => ${data.teacher}
        教学班名称 => ${data.classmate}
        人数 => ${data.mateNum}
        上课类型 => ${data.type}
        授课内容 => ${data.context?data.context:'(空)'}`,
    }).then();
    this.setData({
      dialogShow: true
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let termData = []
    let dateData = []
    let dayData = []

    //生成学期数组
    for (let index = 0; index < 8; index += 2) {
      let date = new Date().getFullYear() - (index / 2);
      termData.push({
        text: date + "02",
        value: index
      });
      termData.push({
        text: date + "01",
        value: index + 1
      });
    }

    //生成周次数组
    for (let index = 1; index < 23; index++) {
      dateData.push({
        value: index,
        text: "第" + index + "周"
      });
    }

    dateData.unshift({
      text: "全部(周次)",
      value: 0
    })

    //生成周几数组
    for (let index = 1; index < 8; index++) {
      dayData.push({
        value: index,
        text: "周" + index
      });
    }

    dayData.unshift({
      text: "全部(周几)",
      value: 0
    })

    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
      option1: termData,
      option2: dateData,
      option3: dayData,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})