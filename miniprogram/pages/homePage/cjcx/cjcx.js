import Dialog from '@vant/weapp/dialog/dialog';
var appInstance = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hei: null,
    maxTop: null,
    showYzm: false,
    yzmSrc: "",
    yzmValue: "",
    dataList: [],
    allResults: {}
  },

  getNew() {  //获取数据
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    if (appInstance.login) {
      this.getData()
    } else {
      wx.cloud.callFunction({ //获取验证码
        name: 'login',
        data: {
          skyw: true
        }
      }).then(res => {
        wx.hideLoading()
        this.setData({
          yzmSrc: res.result.data,
          showYzm: true
        })
      }).catch(err => {
        wx.hideLoading()
        Dialog.alert({
          message: "网络错误，请重试",
        })
        appInstance.login = false
      })
    }
  },
  yzmChange(event) {
    this.setData({
      yzmValue: event.detail
    })
  },
  submit1() {
    wx.showLoading({
      title: '登陆中......',
      mask: true
    })

    wx.cloud.callFunction({ //登陆
      name: 'login',
      data: {
        yzmValue: this.data.yzmValue
      }
    }).then(res => {
      wx.hideLoading()
      if (typeof res.result == "string") { //登陆失败
        if (res.result == "验证码不正确") {
          Dialog.alert({
            message: res.result,
          }).then(() => {

            wx.showLoading({
              title: '更新验证码中......',
              mask: true
            })
            wx.cloud.callFunction({ //获取验证码
              name: 'login',
            }).then(res => {
              wx.hideLoading()
              this.setData({
                yzmSrc: res.result.data,
                showYzm: true
              })
            }).catch(err => Dialog.alert({
              message: "网络错误，请重试",
            }))

          });
        } else { //其它错误
          Dialog.alert({
            message: res.result,
          })
        }

      } else { //登陆成功
        appInstance.login = true;
        wx.showLoading({
          title: '加载中',
          mask: true
        })
        this.getData()
      }
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: "网络错误，请重试",
      })
      appInstance.login = false
    })
  },
  getData() {
    wx.cloud.callFunction({ //获取后台数据
      name: 'cjcx',
      data: {
        getNew: true
      }
    }).then(res => {
      wx.hideLoading()
      if (res.result != "ok") { //出现错误
        Dialog.alert({
          message: '网络错误，请重试',
        })
      } else { //ok 获取了数据
        this.cjcx()
      }
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: '网络错误，请重试',
      })
      appInstance.login = false
    })
  },
  cjcx() {
    wx.cloud.callFunction({ //获取数据
      name: 'cjcx',
      data: {
        getNew: false
      }
    }).then(res => {
      this.setData({
        allResults: res.result[0],
        dataList: res.result[1]
      })
      wx.hideLoading()
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: '网络错误，请重试',
      })
      appInstance.login = false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
    })
    wx.showLoading({
      title: '加载中......',
      mask: true
    })
    this.cjcx()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})