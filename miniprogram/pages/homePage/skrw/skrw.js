import Dialog from '@vant/weapp/dialog/dialog';
var appInstance = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hei: null,
    maxTop: null,
    dataList: [],
    dialogData: {},
    show: false,
    showYzm: false,
    yzmSrc: "",
    yzmValue: ""
  },

  getNew() {
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    if (appInstance.login) { //判断是否登陆
      this.getData()
    } else {
      wx.cloud.callFunction({ //获取验证码
        name: 'login',
        data: {
          skyw: true
        }
      }).then(res => {
        wx.hideLoading()
        this.setData({
          yzmSrc: res.result.data,
          showYzm: true
        })
      }).catch(err => {
        wx.hideLoading()
        Dialog.alert({
          message: "网络错误，请重试",
        })
        appInstance.login = false
      })
    }
  },
  yzmChange(event) {  //验证码
    this.setData({
      yzmValue: event.detail
    })
  },
  submit1() {
    wx.showLoading({
      title: '登陆中......',
      mask: true
    })

    wx.cloud.callFunction({ //登陆
      name: 'login',
      data: {
        yzmValue: this.data.yzmValue
      }
    }).then(res => {
      wx.hideLoading()
      if (typeof res.result == "string") { //登陆失败
        if (res.result == "验证码不正确") {
          Dialog.alert({
            message: res.result,
          }).then(() => {

            wx.showLoading({
              title: '更新验证码中......',
              mask: true
            })
            wx.cloud.callFunction({ //获取验证码
              name: 'login',
            }).then(res => {
              wx.hideLoading()
              this.setData({
                yzmSrc: res.result.data,
                showYzm: true
              })
            }).catch(err => Dialog.alert({
              message: "网络错误，请重试",
            }))

          });
        } else { //其它错误
          Dialog.alert({
            message: res.result,
          })
        }

      } else { //登陆成功
        appInstance.login = true;
        wx.showLoading({
          title: '查询中......',
          mask: true
        })
        this.getData()
      }
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: "网络错误，请重试",
      })
      appInstance.login = false
    })
  },
  getData() {
    wx.cloud.callFunction({ //获取后台数据
      name: 'skrw',
      data: {
        getNew: true
      }
    }).then(res => {
      this.getskrw()
    }).catch(err => {
      wx.hideLoading()
      Dialog.alert({
        message: '网络错误，请重试',
      })
      appInstance.login = false
    })
  },

  dialog(e) {  //弹窗显示数据
    let data = e.target.dataset.data
    this.setData({
      dialogData: data,
      show: true
    })
  },
  close() {
    this.setData({
      show: false
    })
  },

  getskrw() {  //获取数据
    wx.cloud.callFunction({
      name: 'skrw',
      data: {
        getNew: false
      }
    }).then(res => {
      let arr = []
      for (const key in res.result) {
        res.result[key].forEach(item => {
          item.xq = key
          arr.push(item)
        })
      }
      this.setData({
        dataList: arr
      })
      wx.hideLoading()
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
    })
    wx.showLoading({
      title: '加载中......',
      mask: true
    })
    this.getskrw()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})