var appInstance = getApp()
import Toast from '@vant/weapp/toast/toast';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    hei: null,
    maxTop: null,
    option1: [{
      text: "南主楼",
      value: 0
    }, {
      text: "马兰芳楼",
      value: 1
    }, {
      text: "科学馆",
      value: 2
    }, {
      text: "黄浩川楼",
      value: 3
    }, {
      text: "体育馆",
      value: 4
    }, {
      text: "黎耀华楼",
      value: 5
    }, {
      text: "建筑馆",
      value: 6
    }, {
      text: "新会楼",
      value: 7
    }, {
      text: "吕志和科技楼",
      value: 8
    }, {
      text: "活动中心",
      value: 9
    }, {
      text: "台山楼",
      value: 10
    }, {
      text: "五友楼",
      value: 11
    }, {
      text: "伍舜德楼",
      value: 12
    }, {
      text: "黎耀球楼",
      value: 13
    }, {
      text: "电子图书馆",
      value: 14
    }, {
      text: "继续教育学院",
      value: 15
    }, {
      text: "恩平楼",
      value: 16
    }, {
      text: "马兰芳教学楼",
      value: 17
    }, {
      text: "黄浩川教学楼",
      value: 18
    }, {
      text: "陆佑图书馆",
      value: 19
    }, {
      text: "综合实验大楼",
      value: 20
    }, {
      text: "吕志和礼堂",
      value: 21
    }, {
      text: "江门楼",
      value: 22
    }, {
      text: "北主楼",
      value: 23
    }, {
      text: "德胜楼",
      value: 24
    }, {
      text: "马观适运动馆",
      value: 25
    }, {
      text: "陈瑞祺科学馆",
      value: 26
    }, {
      text: "实训基地",
      value: 27
    }, {
      text: "体育场地",
      value: 28
    }, {
      text: "香港台山商会大楼",
      value: 29
    }, {
      text: "十友楼",
      value: 30
    }, {
      text: "学生综合服务中心楼",
      value: 31
    }, {
      text: "伍舜德教授活动中心",
      value: 32
    }, {
      text: "学生活动中心",
      value: 33
    }, {
      text: "综合实训大楼1号楼",
      value: 34
    }],
    option2: [],
    option3: [],
    value1: 0,
    value2: 0,
    value3: 0,
  },
  change1(e) {
    this.data.value1 = e.detail
  },
  change2(e) {
    this.data.value2 = e.detail
  },
  change3(e) {
    this.data.value3 = e.detail
  },
  submit() {
    wx.showLoading({
      title: '查询中......',
      mask: true
    })

    wx.cloud.callFunction({ //获取验证码
      name: 'kjs',
      data: {
        place: this.data.option1[this.data.value1],
        date: this.data.option2[this.data.value2],
        day: this.data.option3[this.data.value3]
      }
    }).then(res => {
      wx.hideLoading()
      if (res.result == 404) {
        Toast.fail("没有找到数据")
      } else {
        this.setData({
          dataList: res.result.data
        })
      }
    })

    this.setData({
      headerShow: true
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let dateData = []
    let dayData = []

    for (let index = 1; index < 23; index++) {
      dateData.push({
        value: index,
        text: "第" + index + "周"
      });
    }

    dateData.unshift({
      text: "全部(周次)",
      value: 0
    })

    for (let index = 1; index < 8; index++) {
      dayData.push({
        value: index,
        text: "周" + index
      });
    }

    dayData.unshift({
      text: "全部(周几)",
      value: 0
    })

    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
      option2: dateData,
      option3: dayData,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})