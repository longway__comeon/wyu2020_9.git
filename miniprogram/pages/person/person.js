var appInstance = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 2,
    hei: null,
    maxTop: null,
    xh: "",
    mm: "",
  },
  tabbarChange(event) { //标签栏变化
    // event.detail 的值为当前选中项的索引
    this.setData({
      active: event.detail
    });
    if (event.detail == 0) {
      wx.redirectTo({
        url: '../home/home'
      })
    } else if (event.detail == 1) {
      wx.redirectTo({
        url: '../classTable/classTable'
      })
    } else if (event.detail == 2) {
      wx.redirectTo({
        url: '../person/person'
      })
    }

  },
  input(event) { //input输入
    let type = event.target.dataset.type
    if (type == "xh") {
      this.setData({
        xh: event.detail
      })
    } else if (type == "mm") {
      this.setData({
        mm: event.detail
      })
    } else {
      this.setData({
        yzm: event.detail
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.cloud.callFunction({
      name: 'user',
      data: {
        type: "getxhmm"
      }
    }).then(res => {
      this.setData({
        xh: res.result.xh,
        mm: res.result.mm
      })
    })
    this.setData({
      hei: appInstance.hei,
      maxTop: appInstance.maxTop,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.cloud.callFunction({ //获取验证码
      name: 'user',
      data: {
        type: "changexhmm",
        xh: this.data.xh,
        mm: this.data.mm
      }
    })

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})