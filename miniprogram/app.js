//app.js
App({
  hei: null,
  maxTop: null,
  login: false,
  onShow: function () {
    const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
    this.hei = menuButtonInfo.top + menuButtonInfo.height + 3
    this.maxTop = menuButtonInfo.top
  },
  onLaunch: function () {

    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        traceUser: true,
      })
    }
  }
})