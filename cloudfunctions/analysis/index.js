const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const _ = db.command

exports.main = async (event, context) => {
  return await db.collection('kb_202001').where({
    id: "1094120",
    teacher: "邹海林"
  }).get()
}


// {
//   term: row[0], //学年学期
//   lessonNum: row[1], //开课编号
//   address: row[2], //学分
//   name: row[3], //课程名称
//   classmeta: row[4], //教学班名称
//   peolpeNum: row[5], //排课人数
//   metaNum: row[6], //上课人数
//   teacher: row[7], //授课教师
//   whatDay: row[8], //星期
//   section: row[9], //节次
//   destination: row[10], //上课地点
//   week: row[11], //周次
//   order: row[12], //课序
//   type: row[13], //教学环节
//   time: row[14], //日期
//   groupName: row[15], //分组名
//   context: row[16] //授课内容简介
// }