// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
  try {
    return await db.collection("kb_" + event.term.text).where({
      name: event.name == '' ? _.neq(event.name) : db.RegExp({
        regexp: event.name,
        options: 'i',
      }),
      week: event.date.value == 0 ? _.neq(event.date.value.toString()) : event.date.value.toString(),
      whatDay: event.day.value == 0 ? _.neq(event.day.value.toString()) : event.day.value.toString(),
      teacher: event.teacher == '' ? _.neq(event.teacher) : db.RegExp({
        regexp: event.teacher,
        options: 'i',
      }),
    }).get()
  } catch (error) {
    return 404
  }


}