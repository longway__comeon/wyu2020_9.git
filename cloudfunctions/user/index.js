// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  if (event.type == "changeselectTerm") {
    await db.collection('user').where({
      id: wxContext.OPENID
    }).update({
      data: {
        selectTerm: event.selectTerm
      }
    })
  } else if (event.type == "getselectTerm") {
    let data = await db.collection('user').where({
      id: wxContext.OPENID
    }).get()
    return data.data[0].selectTerm
  } else if (event.type == "getxhmm") {
    let data = await db.collection('user').where({
      id: wxContext.OPENID
    }).get()
    return {
      "xh": data.data[0].xh,
      "mm": data.data[0].mm
    }

  } else if (event.type == "changexhmm") {
    await db.collection('user').where({
      id: wxContext.OPENID
    }).update({
      data: {
        xh: event.xh,
        mm: event.mm
      }
    })

  }

}